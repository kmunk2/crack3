#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be
const int HASH_LEN=34;


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char plaintext[PASS_LEN];
    char hash[HASH_LEN];
};


// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    
    //printf("Checking if this is where the error is\n");
    
    *size = 0;
    struct entry *output = NULL;
    
    FILE *in = fopen(filename, "r");
    char *temp = NULL;
    
    int notnull = 0;
    
    if(!in){
        return(NULL);
    }
    
    printf("Loading rockyou file contents:\n\n");
    
    do{
        //printf("Checking if this is where the error is\n");
        temp = (char *) malloc(sizeof(char)*PASS_LEN); 
        //printf("Nope\n");
        
        notnull = fgets(temp, PASS_LEN, in) == NULL;
        
        if(notnull){
            
            printf("\n\nEnd of file found\n\n");
            break;
            
        }
        else{
            
            //printf("Read: %s\n", temp);
        
            //observations sizeof the structure
            //printf("The size of the entry structure is %ld\n", sizeof(struct entry));
            
            //first we expand the output array
            if(*size==0){
                
                output = (struct entry *) malloc(sizeof(struct entry)*(*size+1));
                
            }
            else{
                
                output = (struct entry *) realloc(output, sizeof(struct entry) * (*size+1));
                //break;
                
            }
            
            
            *size += 1;
            
            for(int i = 0; i<PASS_LEN; i++){
                
                //printf("Character transferred %d\n");
                
                output[(*size)-1].plaintext[i] = 0;
                if(i < strlen(temp)){
                    int readchar = temp[i];
                    
                    if(readchar != '\n' && readchar != 932 && readchar != 0){
                        output[(*size)-1].plaintext[i] = temp[i];
                    }
                    else{
                        output[(*size)-1].plaintext[i] = 0;
                    }
                }
                
            }
            
            //printf("testing if the problem is here\n");
            char *hash = md5(output[(*size)-1].plaintext, strlen(output[(*size)-1].plaintext));
            //printf("\n\ntested\n");
            
            //printf("testing if the problem is here\n");
            for(int i = 0; i<HASH_LEN; i++){
                
                output[(*size)-1].hash[i] = 0;
                
                if(i < strlen(hash)){
                    output[(*size)-1].hash[i] = hash[i];
                }
                
            }
            //printf("\n\ntested\n");
            //*/
            //printf("Registered: %s\n", output[(*size)-1].plaintext);
            //printf("Hashed as: %s\n", output[(*size)-1].hash);
            
            printf("#");
        }
        
    }while(!notnull);
    
    free(temp);
    //printf("Attempting to return the output\n");
    
    return output;
}

//read sample
void read_sample(struct entry *values){
    for(int i=0; i<10; i++){
        printf("Hash: %s\n", values[i].hash);
    }
}

//the comparison function
int cmpfunc(const void * a, const void * b){
    /*//first I'm going through and casting the unknown variables as a constant structure
    const struct entry* alpha = (const struct entry*)a;
    const struct entry* beta = (const struct entry*)b;
    
    //then I extract the hash file since that is how I will opt to sort this mess
    const char* aa = (*alpha).hash;
    const char* bb = (*beta).hash;
    
    //then I will compare all of the hash files against eachother
    return(strcmp(aa,bb));
    //*/
    
    int comparison = strcmp(((const struct entry*)a)->hash,((const struct entry*)b)->hash);
    
    //printf("Comparison is: %d\n", comparison);
    /*
    int result = 0;
    if(comparison < 0){
        result = -1;
    }
    else if(comparison > 0){
        result = 1;
    }
    
    return(result);
    //*/return(comparison);
}

struct entry *read_hashes(char *filename, int *size)
{
    //yes most of this is a clone of the above read_dictionary function
    //it happens to be a robust system
    //printf("Checking if this is where the error is\n");
    
    *size = 0;
    struct entry *output = NULL;
    
    FILE *in = fopen(filename, "r");
    char *temp = NULL;
    
    int notnull = 0;
    
    if(!in){
        return(NULL);
    }
    
    printf("\nLoading hash file contents:\n\n");
    
    do{
        //printf("Checking if this is where the error is\n");
        temp = (char *) malloc(sizeof(char)*HASH_LEN); 
        //printf("Nope\n");
        
        notnull = fgets(temp, HASH_LEN, in) == NULL;
        
        if(notnull){
            
            printf("\n\nEnd of file found\n\n");
            break;
            
        }
        else{
            
            //printf("Read: %s\n", temp);
        
            //observations sizeof the structure
            //printf("The size of the entry structure is %ld\n", sizeof(struct entry));
            
            //first we expand the output array
            if(*size==0){
                
                output = (struct entry *) malloc(sizeof(struct entry)*(*size+1));
                
            }
            else{
                
                output = (struct entry *) realloc(output, sizeof(struct entry) * (*size+1));
                //break;
                
            }
            
            
            *size += 1;
            
            for(int i = 0; i<HASH_LEN; i++){
                
                //printf("Character transferred %d\n");
                
                output[(*size)-1].hash[i] = 0;
                if(i < strlen(temp)){
                    int readchar = temp[i];
                    
                    if(readchar != '\n' && readchar != 932 && readchar != 0){
                        output[(*size)-1].hash[i] = temp[i];
                    }
                    else{
                        output[(*size)-1].hash[i] = 0;
                    }
                }
                
            }
            
            for(int i = 0; i<PASS_LEN; i++){
                output[(*size) - 1].plaintext[i] = 0;
            }
            
            //printf("testing if the problem is here\n");
            //char *hash = md5(output[(*size)-1].plaintext, strlen(output[(*size)-1].plaintext));
            //printf("\n\ntested\n");
            
            /*//printf("testing if the problem is here\n");
            for(int i = 0; i<HASH_LEN; i++){
                
                output[(*size)-1].hash[i] = 0;
                
                if(i < strlen(hash)){
                    output[(*size)-1].hash[i] = hash[i];
                }
                
            }
            //printf("\n\ntested\n");
            //*/
            //printf("Registered: %s\n", output[(*size)-1].plaintext);
            //printf("Hashed as: %s\n", output[(*size)-1].hash);
            
            printf("#");
        }
        
    }while(!notnull);
    
    free(temp);
    //printf("Attempting to return the output\n");
    
    return output;
    
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    printf("Starting\n");
    
    int dict_len = 0;
    int targ_len = 0;
    
    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &dict_len);
    //printf("Output successful\n");
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    
    if(dict == 0){
        printf("dictionary Error detected");
        exit(1);
    }
    else{
        printf("No dictionary error found\n");
    }
    
    //read_sample(dict);
    
    //looks like I'm supposed to use the size of the struct rather than the size of the comparison data
    //otherwise I'll corrupt my data set in a way that brings true meaning to fubar
    //printf("Sorting\n");
    qsort(dict, dict_len, sizeof(struct entry), cmpfunc);
    
    //read_sample(dict);
    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".
    // I could go about this in two ways, I could search it in the same format or I could create a unique comparison
    // A same format search would be less complex, a unique comparison would be "cleaner" data wise
    // Opting for less complex over data consumption
    /*struct entry searchTerm;
    strcpy(searchTerm.plaintext,"unknown");
    strcpy(searchTerm.hash,"f806fc5a2a0d5ba2471600758452799c");
    struct entry *found = bsearch(&searchTerm, dict, dict_len, sizeof(struct entry), cmpfunc);
    
    printf("Match point found at %d\n", found);
    printf("Cracked password is: %s\n", (*found).plaintext);
    //*/
    
    
    // TODO
    // Open the hash file for reading.
    struct entry *hash = read_hashes(argv[1], &targ_len);
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    for(int i=0; i<targ_len; i++){
        struct entry *found = bsearch(hash+i, dict, dict_len, sizeof(struct entry), cmpfunc);
        printf("Cracked %s as %s\n", (*found).hash, (*found).plaintext);
    }
    
    free(hash);
    free(dict);
    
    return(1);
}
